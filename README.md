# Event generator for choppers

# Startup script for IOC
/epics/iocs/cmds/embla-evg-01/st.iocsh

# General IOC folder
/epics/iocs/cmds/embla-evg-01


# systemd file
/etc/systemd/system/ioc@embla-evg-01.service

# conserver file
/etc/conserver/procs.cf