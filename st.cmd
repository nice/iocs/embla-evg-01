#!/usr/bin/env iocsh.bash
# Select the software release
require(mrfioc2)

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("LOCATION","Embla")
epicsEnvSet("IOC", "LabS-Embla:TS")
epicsEnvSet("DEV", "EVG-01")
epicsEnvSet("ENGINEER","Nicklas Holmberg")
epicsEnvSet("MainEvtCODE" "14")
epicsEnvSet("HeartBeatEvtCODE"   "122")
epicsEnvSet("ESSEvtClockRate"  "88.0525")
#epicsEnvSet("MTCA_3U_PCIID6",  "05:0d.0")


# give execution permission to the find_pci_bus_id.bash

system("./find_pci_bus_id.bash")
< "./pci_bus_id"
epicsEnvSet("PCIID", "$(PCI_BUS_NUM):00.0")

system("git checkout pci_bus_id")

epicsEnvSet("MTCA_3U_PCIID6",  "$(PCI_BUS_NUM):0d.0")

## 3U added epicsEnvSet("MTCA_3U_PCIID6",  "06:0e.0") to env-init.iocsh
#iocshLoad("$(mrfioc2_DIR)/env-init.iocsh")
#iocshLoad("$(mrfioc2_DIR)/evg-mtca-init.iocsh", "IOC=$(IOC), DEV=$(DEV), PCIID=$(MTCA_3U_PCIID6)")
iocshLoad("$(mrfioc2_DIR)/evg-cpci-init.iocsh", "IOC=$(IOC), DEV=$(DEV), PCIID=$(MTCA_3U_PCIID6)")

# load e3-common
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

iocInit()

# RUNTIME
# =======
#iocshLoad("$(mrfioc2_DIR)/evg-mtca-run.iocsh", "IOC=$(IOC), DEV=$(DEV), INTREF=''")
# Add normal operation sequencer0 for tests
#iocshLoad("$(mrfioc2_DIR)/_evg-seq0-run.iocsh", "IOC=$(IOC), DEV=$(DEV)")

# dbl > "${IOC}_PVs.list"

#makeAutosaveFileFromDbInfo("$(AUTOSAVE)/mrf_settings.req",  "autosaveFields_pass0")
#makeAutosaveFileFromDbInfo("$(AUTOSAVE)/mrf_values.req",    "autosaveFields")
#makeAutosaveFileFromDbInfo("$(AUTOSAVE)/mrf_waveforms.req", "autosaveFields_pass1")

#create_monitor_set("mrf_settings.req",   5 , "")
#create_monitor_set("mrf_values.req",     5 , "")
#create_monitor_set("mrf_waveforms.req", 30 , "")

############### Configure RF input ##########
dbpf $(IOC)-$(DEV):EvtClk-RFFreq-SP 88.0525
dbpf $(IOC)-$(DEV):EvtClk-RFDiv-SP 1
dbpf $(IOC)-$(DEV):EvtClk-Source-Sel "RF (Ext)"
# PLL needs time in order to synchronize
epicsThreadSleep 3
############### Configure RF input ##########

############## Configure front panel for evr 125 1 Hz ##############
dbpf $(IOC)-$(DEV):1ppsInp-Sel "Univ0"
dbpf $(IOC)-$(DEV):1ppsInp-MbbiDir_.TPRO 1
dbpf $(IOC)-$(DEV):TrigEvt1-TrigSrc-Sel "Mxc1"
dbpf $(IOC)-$(DEV):TrigEvt1-EvtCode-SP 125
#dbpf $(IOC)-$(DEV):1ppsInp-Sel "Sys Clk"
dbpf $(IOC)-$(DEV):SyncTimestamp-Cmd 1
dbpf $(IOC)-$(DEV):Mxc1-Prescaler-SP 88052500

############## Sequencer Master Event Rate 14 Hz ##############
#dbpf $(IOC)-$(DEV):Mxc0-Prescaler-SP 6289464


# Setup of sequencer
dbpf $(IOC)-$(DEV):SoftSeq0-RunMode-Sel "Normal"
dbpf $(IOC)-$(DEV):SoftSeq0-TrigSrc-Sel "Mxc0"
dbpf $(IOC)-$(DEV):SoftSeq0-TsResolution-Sel "Ticks"
dbpf $(IOC)-$(DEV):SoftSeq0-Load-Cmd 1
dbpf $(IOC)-$(DEV):SoftSeq0-Enable-Cmd 1
## Load the sequence
system("./config_seq_14Hz.sh $(IOC) $(DEV)")
dbpf $(IOC)-$(DEV):SoftSeq0-Commit-Cmd 1

############## Master Event Rate 14 Hz ##############
# # 14 Hz
dbpf $(IOC)-$(DEV):Mxc0-Prescaler-SP 6289464
#dbpf $(IOC)-$(DEV):TrigEvt0-EvtCode-SP $(MainEvtCODE)
#dbpf $(IOC)-$(DEV):TrigEvt0-TrigSrc-Sel "Mxc0"

# # Heart Beat 1 Hz
dbpf $(IOC)-$(DEV):Mxc7-Prescaler-SP 88052500
dbpf $(IOC)-$(DEV):TrigEvt7-EvtCode-SP $(HeartBeatEvtCODE)
dbpf $(IOC)-$(DEV):TrigEvt7-TrigSrc-Sel "Mxc7"

# Set EVM as master (EVG) and run sequences
dbpf $(IOC)-$(DEV):Enable-Sel "Ena Master"

#EOF
