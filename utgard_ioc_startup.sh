#!/bin/bash
eval "$(/opt/conda/condabin/conda shell.bash hook)"
conda config --set channel_alias https://artifactory.esss.lu.se/artifactory/api/conda
conda config --add channels conda-e3-dev-virtual
conda config --add channels ics-conda
# only (re)create the invironment if we can ping the ess artifactory.
# this is to avoid failure on restart if the IOC host loses internet connection.
ping -W 1 -w 1 -c 1 artifactory.esss.lu.se \
# && conda env create --force -n embla-evg-01 -f /epics/iocs/cmds/embla-evg-01/environment.yaml
conda activate embla-evg-01
source "/epics/iocs/cmds/embla-evg-01/utgard_env.sh"
# source user env file if it exists
FILE="/epics/iocs/cmds/embla-evg-01/env.sh" && test -f $FILE && source $FILE
/epics/iocs/cmds/embla-evg-01/st.iocsh
